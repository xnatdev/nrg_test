package org.nrg.testing.latex

interface LatexComponent {

    String inject(String baseContent)

}