package org.nrg.testing.xnat.processing.exceptions

class ImageProcessingException extends FileValidationException {

    ImageProcessingException(String message) {
        super(message)
    }

}
