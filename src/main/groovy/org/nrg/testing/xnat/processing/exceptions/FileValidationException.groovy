package org.nrg.testing.xnat.processing.exceptions

class FileValidationException extends ProcessingValidationException {

    FileValidationException(String message) {
        super(message)
    }

}
