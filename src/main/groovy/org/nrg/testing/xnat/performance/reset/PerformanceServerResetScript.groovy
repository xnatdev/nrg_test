package org.nrg.testing.xnat.performance.reset

interface PerformanceServerResetScript {

    void resetXnatServer()

}
