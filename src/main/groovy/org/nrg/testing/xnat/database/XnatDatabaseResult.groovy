package org.nrg.testing.xnat.database

import java.sql.Connection
import java.sql.ResultSet

class XnatDatabaseResult {

    Connection connection
    int rowCount
    ResultSet resultSet

}
