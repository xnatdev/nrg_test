package org.nrg.testing.dicom

interface InterfileDicomValidation {

    void validate(Map<File, DicomObject> dicomObjectFileMap)

}
