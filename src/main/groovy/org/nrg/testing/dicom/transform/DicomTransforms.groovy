package org.nrg.testing.dicom.transform

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.data.VR
import org.dcm4che3.util.UIDUtils
import org.nrg.testing.DicomUtils

class DicomTransforms {

    public static final TransformFunction REMAP_SOP_INSTANCE_UID = TransformFunction.simple(
            dicom -> dicom.setString(Tag.SOPInstanceUID, VR.UI, UIDUtils.createUID())
    )
    public static final TransformFunction REMAP_UIDS = TransformFunction.composition(
            REMAP_SOP_INSTANCE_UID,
            TransformFunction.strictlyTransformative(listOfDicom -> {
                final Map<String, String> studyMap = [:]
                final Map<String, String> seriesMap = [:]
                listOfDicom.each { instance ->
                    instance.setString(
                            Tag.StudyInstanceUID,
                            VR.UI,
                            studyMap.computeIfAbsent(instance.getString(Tag.StudyInstanceUID), (unused) -> UIDUtils.createUID())
                    )
                    instance.setString(
                            Tag.SeriesInstanceUID,
                            VR.UI,
                            seriesMap.computeIfAbsent(instance.getString(Tag.SeriesInstanceUID), (unused) -> UIDUtils.createUID())
                    )
                    instance.setString(Tag.SOPInstanceUID, VR.UI, UIDUtils.createUID())
                }
            })
    )

    static final TransformFunction duplicateInstance(int numInstances) {
        TransformFunction.composition(
                TransformFunction.generalTransform({ listOfDicom ->
                    if (listOfDicom.size() > 1) {
                        throw new UnsupportedOperationException('This transform expects to take only a single DICOM instance')
                    }
                    (0 ..< numInstances).collect { index ->
                        final Attributes clone = DicomUtils.clone(listOfDicom[0])
                        clone.setInt(Tag.InstanceNumber, VR.IS, index)
                        clone
                    }
                }), // turn 1 instance into N instances with unique Instance Number...and then run those through SOP Instance UID remap
                REMAP_SOP_INSTANCE_UID
        )
    }

}
