package org.nrg.testing.listeners.adapters.jira.failure

interface FailureCause {

    String getHTMLReason()

}
